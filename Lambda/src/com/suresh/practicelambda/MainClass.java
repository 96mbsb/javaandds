package com.suresh.practicelambda;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainClass {
	public static void main(String[] args) {
		List<Persons> personsList = Arrays.asList(new Persons(1,"Suresh"), 
												  new Persons(3,"Kavi"),
												  new Persons(2,"Sam"));
		Runnable printPerson = () -> {
			for(Persons person: personsList)
				System.out.println(person);
		};
		
		System.out.println("Before Sorting");
		printPerson.run();

		Collections.sort(personsList, (o1,o2)-> o2.compareTo(o1));
		System.out.println("\nAfter Sorting");
		printPerson.run();
		
		Runnable printPersonName = () -> {
			for(Persons person: personsList)
				System.out.println(person.getName());
		};
		System.out.println("\nPrint Name only:");
		printPersonName.run();
	}
	
	
}
