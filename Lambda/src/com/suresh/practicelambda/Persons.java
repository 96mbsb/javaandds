package com.suresh.practicelambda;

public class Persons implements Comparable<Persons>{
	private int rollNo;
	private String name;
	
	public Persons(int rollNo, String name) {
		super();
		this.rollNo = rollNo;
		this.name = name;
	}
	
	public int getRollNo() {
		return rollNo;
	}
	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "Persons [rollNo=" + rollNo + ", name=" + name + "]";
	}

	@Override
	public int compareTo(Persons o) {
		return this.rollNo>o.rollNo?-1: this.rollNo<o.rollNo?1:0;
	}


}
