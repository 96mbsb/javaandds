package com.suresh.lambda;

public class RunnableExample {
	public static void main(String[] args) {
		Thread t =new Thread(()->System.out.println("Thread is running"));
		t.start();
	}
}
