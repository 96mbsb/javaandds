package com.suresh.lambda;

@FunctionalInterface
public interface HelloWorld {
	public String showMessage(int i, int j);
}
