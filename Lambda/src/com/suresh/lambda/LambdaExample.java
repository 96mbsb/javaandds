package com.suresh.lambda;

public class LambdaExample {
	public static void main(String[] args) {
		 HelloWorld blockOfCode = (a,b) -> "Sum:"+a+b;
		 String result = blockOfCode.showMessage(2,3);
		 System.out.println(result);
	}
}
