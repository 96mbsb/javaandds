package com.suresh.exception;

import java.util.function.BiConsumer;

public class LambdaException {
	public static void main(String[] args) {
		Integer[] numbers= {1,2,3,4};
		Integer divideBy =0;

		process(numbers, divideBy, tryLambda( (number, divBy)-> System.out.println(number/divBy) ));
	}

	private static BiConsumer<Integer,Integer> tryLambda(BiConsumer<Integer,Integer> print) {
		return (number, divBy)-> {
			try {
				print.accept(number, divBy);
			}
			catch(ArithmeticException e){
				System.out.println(e);
			}
		};
	}

	private static void process(Integer[] numbers, Integer divideBy, BiConsumer<Integer,Integer> division) {
		for(Integer number: numbers)
			division.accept(number, divideBy);
	}
}
