package com.suresh.foreach;

import java.util.Arrays;
import java.util.List;

import com.suresh.practicelambda.Persons;

public class ForEach {
	public static void main(String[] args) {
		List<Persons> personsList = Arrays.asList(new Persons(1,"Suresh"), 
				new Persons(3,"Kavi"),
				new Persons(2,"Sam"));
		
		//method reference
		personsList.forEach(System.out::println);
	}	
}
