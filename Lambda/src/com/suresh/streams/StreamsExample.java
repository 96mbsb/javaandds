package com.suresh.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import com.suresh.practicelambda.Persons;

public class StreamsExample {
	public static void main(String[] args) {
		List<Persons> personsList = Arrays.asList(new Persons(1,"Suresh"), 
												  new Persons(3,"Kavi"),
												  new Persons(2,"Sam"));
		
		Stream<Persons> streamPersons = personsList.stream();
		Optional<Persons> p = streamPersons.filter(person -> person.getName().startsWith("S"))
		.findFirst();
		
		System.out.println(p.get());
	}
}
