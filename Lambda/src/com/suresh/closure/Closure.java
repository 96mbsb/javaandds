package com.suresh.closure;

public class Closure {
	public static void main(String[] args) {
		int a=10, b=20, c=30;
		
		//method reference
		Process p = Closure::sum;
		p.process(a,b);
		
		//Closure
		Process p1 = (a1,b1) -> System.out.println("Sum of three is: "+ (a1+b1+c));
		p1.process(a, b);
		
		//this cannot be used in static method containing lambda
	}
	
	interface Process{
		void process(int a, int b);
	}
	
	public static void sum(int a, int b) {
		System.out.println("Sum of two is: "+(a+b));
	}
}
