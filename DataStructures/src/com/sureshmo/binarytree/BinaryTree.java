package com.sureshmo.binarytree;

import java.util.Arrays;

public class BinaryTree <T>{
	@SuppressWarnings("hiding")
	class Node<T>{
		private T value;
		private Node<T> left;
		private Node<T> right;

		public Node(T value) {
			this.value = value;
		}
	}

	private Node <T> root;
	@Override
	public String toString() {
//				String tree = preOrderTraversal(root);
		//		String tree = postOrderTraversal(root);
		//		String tree = inOrderTraversal(root);
				String tree = depthFirstSearch(root);
		return tree;
	}

	private String depthFirstSearch(Node<T> node) {
		String tree = "";
		tree+=node.value;
		tree += printleftAndRight(node.left,node.right);
		return tree;
	}

	private String printleftAndRight(Node<T> left, Node<T> right) {
		if(left!=null && right!=null)
			return " "+left.value.toString() +" "+ right.value.toString() +printleftAndRight(left.left, left.right) + printleftAndRight(right.left, right.right);
		else if(left!=null && right==null)
			return " "+left.value.toString()+ printleftAndRight(left.left, left.right);
		else if(left==null && right!=null)
			return " "+right.value.toString()+printleftAndRight(right.left, right.right);
		else
			return "";
	}
	
	

//	private String preOrderTraversal(Node<T> node) {
//		String tree = "";
//		if(node!=null)
//		{
//			tree+= " " + node.value 
//					+ preOrderTraversal(node.left)
//					+ preOrderTraversal(node.right);
//		}
//		return tree;
//	}
//
//
//	private String postOrderTraversal(Node<T> node) {
//		String tree = "";
//		if(node!=null)
//		{
//			tree+= " " + postOrderTraversal(node.left)
//			+ postOrderTraversal(node.right)
//			+ node.value ;
//		}
//		return tree;
//	}
//
//	private String inOrderTraversal(Node<T> node) {
//		String tree = "";
//		if(node!=null)
//		{
//			tree+= " " + inOrderTraversal(node.left)
//			+ node.value 
//			+ inOrderTraversal(node.right);
//
//		}
//		return tree;
//	}

	public void add(T value) {
		if (root == null)
			root = new Node<T>(value);
		else
		{
			addValue(value, root);
		}
	}

	private void addValue(T value, Node<T> currentNode) {
		Node<T> node;
		if ((Integer)value < (Integer)currentNode.value) {
			node = currentNode.left;
			if(currentNode.left == null)
				currentNode.left = new Node<T>(value);
			else
				addValue(value, currentNode.left);

		}
		else if((Integer)value > (Integer)currentNode.value){
			node = currentNode.right;
			if(currentNode.right == null)
				currentNode.right = new Node<T>(value);
			else
				addValue(value, currentNode.right);
		}
		else {
			return;
		}

	}

	public boolean search(T value) {
		return searchNode(value,root);
	}

	private boolean searchNode(T value, Node<T> node) {
		if(node.value == value)
			return true;
		else if((Integer)node.value < (Integer)value)
			node = node.left;
		else
			node = node.right;

		if(node == null)
			return false;
		else
			return searchNode(value, node);
	}

}
