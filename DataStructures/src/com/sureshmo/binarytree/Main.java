package com.sureshmo.binarytree;

public class Main {
	public static void main(String[] args) {
		BinaryTree<Integer> binaryTree = new BinaryTree<>();
		binaryTree.add(7);
		binaryTree.add(2);
		binaryTree.add(1);
		binaryTree.add(4);
		binaryTree.add(9);
		binaryTree.add(8);
		binaryTree.add(10);
		System.out.println(binaryTree);
		
		System.out.println(binaryTree.search(3));
		System.out.println(binaryTree.search(9));

	}
}
