package com.suresh.stack;

public class Main {
	public static void main(String[] args) {
//		String value = "Reverse";
		
//		ReversingString reverse = new ReversingString();
//		System.out.println(reverse.reverse(value));
//		
//		String validExpression = "{((1+2))}";
//		EvaluateExpression evaluate = new EvaluateExpression();
//		if(evaluate.evaluate(validExpression)) {
//			System.out.println("Valid Expression");
//		}else {
//			System.out.println("Invalid Expression");
//		}
		
		ArrayStack<Integer> stack = new ArrayStack<>();
		stack.push(1);
		stack.push(2);
		stack.push(3);
		System.out.println(stack);
		System.out.println("Pop: "+stack.pop());
		System.out.println(stack);
		System.out.println("Peek: "+stack.peek());
		System.out.println(stack);
		System.out.println("Is empty: "+stack.isEmpty());
	}
}
