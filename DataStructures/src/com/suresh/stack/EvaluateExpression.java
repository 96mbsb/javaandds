package com.suresh.stack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class EvaluateExpression {
	private final ArrayList<Character> openingList = (ArrayList<Character>) Arrays.asList('(','{','<','[');
	private final ArrayList<Character> endingList = (ArrayList<Character>) Arrays.asList(')','}','>','}');
	
	public boolean evaluate(String input) {
		if(input == null)
			throw new IllegalArgumentException();
		
		Stack<Character> stack = new Stack<>();
		for(Character character : input.toCharArray())
		{
			if(openingList.contains(character)){
		       	stack.push(character);
		    }else if(endingList.contains(character)) {
		       	if(stack.isEmpty())
		       		return false;
		       	else{
		       		if(endingList.indexOf(character)!=openingList.indexOf(stack.pop()))
		       			return false;
		       	}
		    }
		}
		return stack.isEmpty();
	}
}
