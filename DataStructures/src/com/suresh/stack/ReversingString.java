package com.suresh.stack;

import java.util.Stack;

public class ReversingString {
	
	public String reverse(String input) {
		Stack<Character> myStack = new Stack<>();
		StringBuffer reversedValue=new StringBuffer();
		
		if(input == null)
			throw new IllegalArgumentException();
		for(Character character : input.toCharArray())
			myStack.push(character);

		//since we are appending and string is immutable number of objects may be created in memory
		while(!myStack.isEmpty())
			reversedValue.append(myStack.pop());
	
		return reversedValue.toString();	
	}

}
