package com.suresh.stack;

import java.util.Arrays;

public class ArrayStack<T> {
	@SuppressWarnings("unchecked")
	private T[] stack = (T[])new Object[10];
	private int index;
	
	public void push(T value) {
		if (index == 10)
			throw new StackOverflowError();
		stack[index++] = value;
	}
	 
	public T pop() {
		if (index == 0)
			throw new IllegalStateException();
		T value = stack[index-1];
		stack[index--]=null;
		return value;
	}
	
	public T peek() {
		if (index == 0)
			throw new IllegalStateException();
		return stack[index-1];
	}
	
	public boolean isEmpty() {
		return index==0?true:false;
	}
	
	@Override
	public String toString() {
		T[] newStack = Arrays.copyOfRange(stack, 0, index);
		return Arrays.deepToString(newStack);
	}
}
