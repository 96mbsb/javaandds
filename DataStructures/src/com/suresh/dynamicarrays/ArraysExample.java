package com.suresh.dynamicarrays;

public class ArraysExample {
	
	public static void main(String[] args) {
		Array<Integer> myArray = new Array<>(3);
		myArray.add(1);
		myArray.add(2);
		System.out.println("Size:"+myArray.size() + " Length:"+myArray.length );
		System.out.println(myArray);
		
		myArray.add(3);
		myArray.add(4);
		System.out.println("Size:"+myArray.size() + " Length:"+myArray.length );
		System.out.println(myArray);
		
		System.out.println("Index of 4:"+myArray.indexOf(4));
		System.out.println("Index of 5:"+myArray.indexOf(5));
		
		myArray.removeAt(3);
		System.out.println("After removal of element at index 3");
		System.out.println("Size:"+myArray.size() + " Length:"+myArray.length );
		System.out.println(myArray);
		
		myArray.remove(1);
		System.out.println("After removal of element1");
		System.out.println("Size:"+myArray.size() + " Length:"+myArray.length );
		System.out.println(myArray);
	}
}
