package com.suresh.dynamicarrays;


public class Array<T> {
	public int length = 0;
	private int size = 0;
	T[] arrayList;
	
	@SuppressWarnings("unchecked")
	public Array(int size) {
		this.size = size;
		arrayList = (T[]) new Object[size];
	}
	
	public void add(T value)
	{
		if(length < size)
		{
			addElement(value);
		}
		else {
			size*=2;
			@SuppressWarnings("unchecked")
			T [] newArrayList = (T[]) new Object[size];
			for(int iterator =0; iterator< size/2; iterator++)
			{
				newArrayList[iterator] = arrayList[iterator];
			}
			arrayList = newArrayList;
			addElement(value);
		}	
	}
	
	public void removeAt(int index) {
		if(0<=index && index<length)
		{
			removeElement(index);
		}
		else {
			throw new ArrayIndexOutOfBoundsException();
		}
	}
	

	public int remove(T element) {
		for(int i=0;i<length ; i++)
		{
			if (arrayList[i].equals(element))
			{
				removeElement(i);
				return i;
			}
		}
		return -1;
	}
	
	public int size() {
		return size;
	}
	
	public int indexOf(T element) {
		
		for(int i=0;i<length;i++)
		{
			if(element.equals(arrayList[i]))
				return i;
		}
		return -1;
	}

	@Override
	public String toString() {
		String values="[";
		
		for(int i=0;i<length;i++)
			values+=arrayList[i];
		values+="]";
		
		return values;
	}

	private void removeElement(int index) {
		for(int i= index; i<length; i++) {
			arrayList[i] = arrayList[i+1];
		}
		arrayList[length] = null;
		length--;
	}

	private void addElement(T value) {
		arrayList[length++] = value;
	}

}
