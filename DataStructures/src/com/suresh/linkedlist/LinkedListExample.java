package com.suresh.linkedlist;

public class LinkedListExample {
	public static void main(String[] args) {
		LinkedList<Integer> linkedList = new LinkedList<>();
		linkedList.addLast(1);
		linkedList.addLast(2);
		linkedList.addLast(3);
		linkedList.addFirst(4);
		linkedList.addFirst(5);
		linkedList.addLast(6);
		linkedList.addAt(6, 8);
		
		System.out.println("After addition:");
		linkedList.print();
		System.out.println("Size: "+linkedList.getSize());
		
		
		linkedList.removeLast();
		linkedList.removeFirst();
		System.out.println("After removal:");
		linkedList.print();
		System.out.println("Size: "+linkedList.getSize());
		
		linkedList.removeAt(2);
		System.out.println("After removal at 2nd position");
		linkedList.print();
		System.out.println("Size: "+linkedList.getSize());
		
		System.out.println("Index of 3 is: " + linkedList.indexOf(3));
		System.out.println("Contains 6?" + linkedList.contains(6));
		System.out.println("Contains 80?" + linkedList.contains(80));
		
		linkedList.reverse();
		System.out.println("After reversing");
		linkedList.print();
		
		System.out.println("Element at distance 4 from last: " + linkedList.getKthElementfromlast(1));
	}
}
