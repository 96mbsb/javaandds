package com.suresh.linkedlist;

import java.util.NoSuchElementException;

public class LinkedList<T> {
	
	@SuppressWarnings("hiding")
	private class Node<T>{
		private T value;
		private Node<T> next;
		
		public Node(T value) {
			this.value = value;
		}
	}

	private Node<T> first;
	private Node<T> last;
	private int size;
	
	public void addLast(T value) {
		Node<T> node;
		if(isEmpty()) {			
			node = new Node<>(value);
			first = last = node;
		}
		else {
			node = new Node<>(value);
			last.next = node;
			last = node;
		}
		size++;
	}
	
	public void addFirst(T value) {
		Node<T> node;
		if(isEmpty()) {			
			node = new Node<>(value);
			first = last = node;
		}
		else {
			node = new Node<>(value);
			node.next = first;
			first = node;
		}
		size++;
	}
	
	private boolean isEmpty() {
		return first==null;
	}

	public void addAt(int index, T value) {
		if(index>=0 && index<=size)
		{
			if(index==0) addFirst(value);
			else if (index==size) addLast(value);
			else {
				Node<T> newNode = new Node<>(value);
				Node<T> previousNode = first;
				for(int i=0;i<index-1;i++)
				{
					previousNode = previousNode.next;
				}
				newNode.next = previousNode.next;
				previousNode.next = newNode;
				size++;
			}
		}
		else {
			throw new IndexOutOfBoundsException();
		}
		
	}
	
	public void removeFirst() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		}
		
		if(first == last)
		{
			first =null;
			last = null;
		}else {
			Node<T> tempNode = first.next;
			first.next = null;
			first = tempNode;
		}
		size--;
	}
	
	public void removeLast() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		}
		
		if(first == last)
		{
			first =null;
			last = null;
		}else {
			Node<T> iterator = first;
			while(iterator != last) {
				if(iterator.next == last)
				{
					iterator.next = null;
				  	last=iterator;
					continue;
				}
				iterator = iterator.next;
			}
		}
		size--;
	}

	public void removeAt(int index) {
		if(index>=0 && index<=size)
		{
			if(index==0) removeFirst();
			else if(index==size) removeLast();
			else {
				Node<T> tempNode;
				Node<T> previousNode = first;
				for(int i=0;i<index-1;i++)
				{
					previousNode = previousNode.next;
				}
				tempNode= previousNode.next;
				previousNode.next = previousNode.next.next;
				tempNode.next = null;
				size--;
			}
			
		}else {
			throw new IndexOutOfBoundsException();
		}
	}
	
	public void print() {
		if(isEmpty())
			throw new IndexOutOfBoundsException();
		else
		{
			Node<T> iterator = first;
			while(iterator != null) {
				System.out.println(iterator.value);
				iterator = iterator.next;
			}
		}
	}

	public int indexOf(T element) {
		int index = 0;
		Node<T> iterator = first;
		while(iterator != null) {
			if (iterator.value == element)
				return index;
			index++;
			iterator = iterator.next;
		}
		return -1;
	}

	public int getSize() {
		return size;
	}
	
	public boolean contains(T value) {
		return this.indexOf(value)!=-1;
	}
	
	public void reverse(){
		if(!isEmpty())
		{
			if (!( first == last) ){
				Node<T> previousNode = null;
				Node<T> currentNode = first;
				Node<T> nextNode;
				while(currentNode != null) {
					nextNode = currentNode.next;
					currentNode.next = previousNode;
					previousNode = currentNode;
					currentNode = nextNode;
				}
				
				last = first;
				first = previousNode;
			}
		}else {
			return;
		}
	}

	public T getKthElementfromlast(int k) {

		
		if(isEmpty()) {
			throw new NullPointerException();
		}
		else if(k<=0 || k>size) {
			throw new IllegalArgumentException();
		}
		else{
			if(k==size)
				return first.value;
			
			Node<T> firstPointer = first;
			Node<T> secondPointer = firstPointer;
			for(int i=1;i<k; i++)
			{
				secondPointer = secondPointer.next;
			}
			
			while(true) {
				if(secondPointer == last)
				{
					return firstPointer.value;
				}
				firstPointer = firstPointer.next;
				secondPointer = secondPointer.next;
			}
		}
		
	}
}
