package com.suresh.hashtable;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;

public class HashTable <R,T>{
	@SuppressWarnings("unchecked")
	private List<Entry<R,T>>[] list = new ArrayList[10];
	private Entry<R,T> entry;
	private int hashValue;
	private int index;

	public void put(R key, T value) {
		entry = new AbstractMap.SimpleEntry<R, T>(key,value);
		hashValue = (Integer)key%10;
		hashValue = getNonFilledSlot(hashValue);
		
		if (list[hashValue] == null)
		{
			list[hashValue] = new ArrayList<>();
			list[hashValue].add(0,entry);
		}
		else {
			updateIndex(key, hashValue);

			if(index == list[hashValue].size())
				list[hashValue].add(index,entry);
			else
				list[hashValue].set(index, entry);
		}

	}

	private void updateIndex(R key, int hashValue) {
		for(index=0;index<list[hashValue].size(); index++)
			if(list[hashValue].get(index).getKey() == key)
				break;
	}

	private int getNonFilledSlot(int hashValue) {
		if (hashValue>10)
			throw new ArrayIndexOutOfBoundsException();

		if(list[hashValue]==null || list[hashValue].size() <= 10)
			return hashValue;

		return getNonFilledSlot(++hashValue);
	}

	@Override
	public String toString() {
		return "HashTable [list=" + Arrays.toString(list) + "]";
	}


}
