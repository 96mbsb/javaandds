package com.suresh.hashtable;

public class Main {
	public static void main(String[] args) {
		HashTable<Integer,String> hTable = new HashTable<>();
		hTable.put(1, "Suresh");
		hTable.put(2, "Kavi");
		hTable.put(3, "Somesh");
		hTable.put(1, "Babu");
		hTable.put(11, "Suresh Babu");
		hTable.put(11, "Suresh Babu");
		hTable.put(21, "Suresh Babu");
		hTable.put(31, "Suresh Babu");
		hTable.put(41, "Suresh Babu");
		hTable.put(51, "Suresh Babu");
		hTable.put(61, "Suresh Babu");
		hTable.put(71, "Suresh Babu");
		hTable.put(81, "Suresh Babu");
		hTable.put(91, "Suresh Babu");
		hTable.put(101, "Suresh Babu");
		hTable.put(111, "Suresh Babu");
		
		System.out.println(hTable);
	}
}
