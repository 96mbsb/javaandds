package com.suresh.hashtable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NonRepeatedCharacters {
	private static List <Character> oneOccuranceCharacters;
	private static List <Character> moreOccuranceCharacters;
	
	public static void main(String[] args) {
		String value = "Suuresh";
		System.out.println(getFirstNonRepeatedChar(value.toLowerCase()));
		System.out.println(getFirstRepeatedChar(value));
	}

	private static Character getFirstNonRepeatedChar(String value) {
		oneOccuranceCharacters=  new ArrayList<>(value.length());
		moreOccuranceCharacters= new ArrayList<>(value.length());
		
		for(Character character : value.toCharArray())
		{
			if(isRepeated(character))
			{
				if(oneOccuranceCharacters.contains(character))
				{
					oneOccuranceCharacters.remove(character);
					moreOccuranceCharacters.add(character);
				}
			}	
			else {
				oneOccuranceCharacters.add(character);
			}
		}

		return oneOccuranceCharacters.get(0);
	}
	
	private static boolean isRepeated(Character character) {
		return oneOccuranceCharacters.contains(character) || moreOccuranceCharacters.contains(character);
	}

	private static Set<Character> set;
	private static Character getFirstRepeatedChar(String value) {
		set= new HashSet<>(value.length());
		for(Character character : value.toCharArray())
		{
			if(set.contains(character))
			{
				return character;
			}	
			set.add(character);
		}
		return null;
	}
	
//	private static HashMap<Character,Integer> characterCountMap = new HashMap<>();
//	private static Character getFirstRepeatedCharUsingMap(String value) {	
//		for(Character character : value.toCharArray())
//		{
//			if(characterCountMap .containsKey(character)) {
//				characterCountMap.put(character, characterCountMap.get(character)+1);
//			}
//			else {
//				characterCountMap.put(character, 0);
//			}
//		}
//		
//		
//		for(Character character : value.toCharArray())
//			if(characterCountMap.get(character).equals(0)) 
//				return character;
//	
//		return null;
//	}
}
