package com.suresh.queue;

import java.util.Queue;
import java.util.Stack;

public class Main {
	public static void main(String[] args) {
//		Queue<Integer> queue = new ArrayDeque<>();
//		queue.add(10);
//		queue.add(20);
//		queue.add(30);
//		
//		queue = reverse(queue);
//		System.out.println(queue);
//		
//
//		ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
//		arrayQueue.enqueue(1);
//		arrayQueue.enqueue(2);
//		arrayQueue.enqueue(3);
//		arrayQueue.enqueue(5);
//		arrayQueue.dequeue();
//		arrayQueue.dequeue();
//		arrayQueue.enqueue(1);
//		arrayQueue.enqueue(2);
//		System.out.println(arrayQueue);
		
		
//		StackQueue<Integer> stackQueue = new StackQueue<>();
//		stackQueue.enqueue(1);
//		stackQueue.enqueue(2);
//		stackQueue.enqueue(3);
//		stackQueue.enqueue(5);
//		System.out.println(stackQueue);
//		System.out.println("Dequeue "+stackQueue.dequeue());
//		System.out.println("Dequeue "+stackQueue.dequeue());
//		System.out.println(stackQueue);
//		stackQueue.enqueue(1);
//		stackQueue.enqueue(2);
//		System.out.println(stackQueue);
		
		PriorityQueue<Integer> priorityQueu = new PriorityQueue<>();
		priorityQueu.enqueue(1);
		priorityQueu.enqueue(2);
		priorityQueu.enqueue(1);
		priorityQueu.enqueue(5);
		System.out.println(priorityQueu);
		priorityQueu.enqueue(1);
		priorityQueu.enqueue(2);
		System.out.println(priorityQueu);
	}

	@SuppressWarnings("unused")
	private static Queue<Integer> reverse(Queue<Integer> queue) {
		Stack<Integer> stack = new Stack<>();
		while(!queue.isEmpty()) 
			stack.push(queue.remove());
			
		while(!stack.isEmpty())
			queue.add(stack.pop());
		return queue;
	}
}
