package com.suresh.queue;

import java.util.Arrays;

public class ArrayQueue<T> {
	@SuppressWarnings("unchecked")
	private T[] queue = (T[])new Object[4];
	private int front;
	private int rear;
	private int count;
	
	public void enqueue(T value) {
		System.out.println("Before Enqueue rear: " + rear);
		if(count == 4)
			throw new IndexOutOfBoundsException();
		
		queue[rear] = value;
		rear= (rear+1)%4;
		count++;
		System.out.println("After Enqueue queue: " + Arrays.toString(queue));
		System.out.println("After Enqueue rear: " + rear);
	}
	
	public T dequeue()
	{
		if(count ==0)
			throw new IllegalStateException();
		
		T value = queue[front];
		queue[front] = null;
		front= (front+1)%4;
		count --;
		return value;
	}
	
	@Override
	public String toString(){
		return Arrays.toString(queue);
	}
}
