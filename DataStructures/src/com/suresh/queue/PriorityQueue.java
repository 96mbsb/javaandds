package com.suresh.queue;

import java.util.Arrays;

public class PriorityQueue<T> {
	@SuppressWarnings("unchecked")
	private T [] values = (T[])new Object[10];
	private int count;
	
	public void enqueue(T value) {
		if(count == 0)
		{
			values[count++] = value;
		}
		else {
			for(int i=count-1; i>=0; i--)
			{
				if((Integer)value >= (Integer)values[i]) {
					values[i+1] =value;
					count++;
					break;
				}
				else {
					values[i+1] = values[i];
				}
			}
		}
	}

	@Override
	public String toString() {
		return "PriorityQueue [values=" + Arrays.toString(values) + "]";
	}
	
//	public T dequeue()
//	{
//		
//	}

}
