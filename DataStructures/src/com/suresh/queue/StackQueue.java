package com.suresh.queue;

import java.util.Stack;

public class StackQueue<T> {
	Stack <T> firstStack = new Stack<>();
	Stack <T> secondtStack = new Stack<>();
	
	public void enqueue(T value) {
		firstStack.push(value);
	}
	
	public T dequeue()
	{
		if (isEmpty())
			throw new IllegalStateException();
		
		if(secondtStack.isEmpty())
		while(!firstStack.isEmpty())
			secondtStack.push(firstStack.pop());
		
		return secondtStack.pop();

	}

	public boolean isEmpty() {
		return firstStack.isEmpty() && secondtStack.isEmpty();
	}

	@Override
	public String toString(){
		
		String values = "[ ";

		for(int iterator=0;iterator< secondtStack.size(); iterator++) 
			values+= " "+secondtStack.get(iterator);
		for(int iterator=0;iterator< firstStack.size(); iterator++) 
			values+= " "+firstStack.get(iterator);
		
		values+=" ]";
		
		return values;
	}
	
}
