package com.suresh.list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class MyList {
	public static void main(String args[]) {
		List<String> arrayList = new ArrayList<>();
		arrayList.add("John");
		arrayList.add("James");
		arrayList.add("Athi");
		arrayList.add("Bala");
		arrayList.add("Mohammed");
		arrayList.add("Kamal");
		
		System.out.println("\nArrayList");
		for(String value: arrayList) {
			System.out.println(value);
		}
		
		//Creating linked list from existing arraylist
		List<String> linkedList = new LinkedList<>(arrayList);
		linkedList.add("Suresh");
		
		System.out.println("\nLinked List");
		for(String value: linkedList) {
			System.out.println(value);
		}
		
		//Default sorting
		Collections.sort(arrayList);
		System.out.println("\nSortedArrayList");
		for(String value: arrayList) {
			System.out.println(value);
		}
		
		//Customized sorting
		Collections.sort(linkedList, (o1,o2) ->{
				return o2.compareTo(o1);
		});
		
		System.out.println("\nReverseSortedLinkedList");
		for(String value: linkedList) {
			System.out.println(value);
		}
	}
}
