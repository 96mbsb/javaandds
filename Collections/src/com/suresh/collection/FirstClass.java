package com.suresh.collection;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FirstClass {
	public static void main(String args[]) {
		System.out.println("Hello World!");
		MyList<Integer> list = new MyList<>();
		list.add(1);
		list.add(2);
		list.add(4);
		list.add(5);
		list.add(3);
		list.add(2);		
		
		MyList<Integer> list1 = new MyList<>();
		list1.add(9);
		
		List<MyList<Integer>> listValues = new ArrayList<>();
		listValues.add(list);
		listValues.add(list1);
		
		Collections.sort(listValues, (Object o1, Object o2)  -> {
				return -1;
		});
		System.out.println(list);
		System.out.println(list1);
		System.out.println(listValues);
	}
}
