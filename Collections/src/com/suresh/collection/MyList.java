package com.suresh.collection;

import java.util.Arrays;
import java.util.Iterator;

public class MyList <T>implements Iterable<T>, Comparable<MyList<T>>{
	@SuppressWarnings("unchecked")
	private Object values[] = (T[])new Object[10];
	private int length;

	void add(T value) {
		values[length]=value;
		length++;
	}
	
	@SuppressWarnings("unchecked")
	T get(int index)
	{
		return (T) values[index];
	}
	
	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {
		int index;
		@Override
		public boolean hasNext() {
			return index<length?true:false;
		}

		@SuppressWarnings("unchecked")
		@Override
		public T next() {
			return (T) values[index++];
		}	
		};
	}
	
	@Override
	public String toString() {
		return "MyList [values=" + Arrays.toString(values) + "]";
	}
	
	@Override
	public int compareTo(MyList<T> o) {
		return this.length< o.length?-1 : this.length> o.length?1 :0;
	}


}
