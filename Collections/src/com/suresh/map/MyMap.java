package com.suresh.map;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class MyMap {
	public static void main(String[] args) {
		Map<Integer,String> hashMap= new HashMap<>();
		hashMap.put(1,"Alan");
		hashMap.put(2,"Bala");
		hashMap.put(4,"Fiaz");
		hashMap.put(3,"Muthu");
		
		Map<Integer,String> linkedHashMap= new LinkedHashMap<>();
		linkedHashMap.put(1,"Alan");
		linkedHashMap.put(2,"Bala");
		linkedHashMap.put(4,"Fiaz");
		linkedHashMap.put(3,"Muthu");
		
		
		Set<Integer> keySet = hashMap.keySet();
		System.out.println("Printing HashMap using Key Set:");
		for(Integer key: keySet) {
			System.out.println("Key:"+key+" Value:"+hashMap.get(key));
		}
		
		System.out.println("\nPrinting LinkedHashMap using Key Set:");
		keySet = linkedHashMap.keySet();
		for(Integer key: keySet) {
			System.out.println("Key:"+key+" Value:"+linkedHashMap.get(key));
		}
		
		Set<Entry<Integer, String>> entrySet = hashMap.entrySet();
		System.out.println("\nPrinting using Entry Set:");
		for(Entry<Integer, String> entry : entrySet) {
			System.out.println("Key:"+entry.getKey()+" Value:"+entry.getValue());
		}
		
		SortedMap<Integer,String> sortedMap = new TreeMap<>(new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return -o1.compareTo(o2);
			}
		});
		
		sortedMap.putAll(hashMap);
		
		System.out.println("\nReverse Sorted Map:");
		Set<Entry<Integer, String>> sortedEntrySet = sortedMap.entrySet();
		for(Entry<Integer, String> entry : sortedEntrySet) {
			System.out.println("Key:"+entry.getKey()+" Value:"+entry.getValue());
		}
	}
}
