package com.suresh.set;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class MySet {
	public static void main(String[] args) {
		Set<String> hashSet = new HashSet<>();
		hashSet.add("Adam");
		hashSet.add("Bradman");
		hashSet.add("Kalis");
		hashSet.add("Dravid");
		
		System.out.println("Hash Set:");
		for(String value: hashSet) {
			System.out.print(value+ " ");
		}
		
		Set<String> linkedHashSet = new LinkedHashSet<>();
		linkedHashSet.add("Adam");
		linkedHashSet.add("Bradman");
		linkedHashSet.add("Kalis");
		linkedHashSet.add("Dravid");
		
		System.out.println("\nLinked Hash Set:");
		for(String value: linkedHashSet) {
			System.out.print(value+ " ");
		}
		
		SortedSet<String> treeSet = new TreeSet<>();
		treeSet.add("Adam");
		treeSet.add("Bradman");
		treeSet.add("Kalis");
		treeSet.add("Dravid");
		
		System.out.println("\nTree Set:");
		for(String value: treeSet) {
			System.out.print(value+ " ");
		}
		
		SortedSet<String> reversedTreeSet = new TreeSet<>((o1,o2) ->{
			return o2.compareTo(o1);
		});
		
		reversedTreeSet.add("Adam");
		reversedTreeSet.add("Bradman");
		reversedTreeSet.add("Kalis");
		reversedTreeSet.add("Dravid");
		
		System.out.println("\nReversed Tree Set:");
		for(String value: reversedTreeSet) {
			System.out.print(value+ " ");
		}
	}
}
